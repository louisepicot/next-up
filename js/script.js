


let data = [],
    arrayNumberViews = [],
    ratingLikes = [],
    allVideosInfo = [],
    heightDisplay = 100,
    maxViewsNumber,
    minViewsNumber,
    count = 1,
    arrayCommentPourcentage = [];



fetch(`${window.location.href}/data.json`)
    .then(response => response.json())
    .then((data) => {
        browseData(data)
    });


    // Le but était de suivre l'algorythme de suggestion de youtube.
    // Comme j'ai néttoyé mon navigateur sinon j'aurais surement eu des recommandations de la villa des coeurs brisés.
    // Le sujet le brexit,
    // Lecture sur l'orientation des porgram
    // comment rendre visuelement qq ChannelSplitterNode, possibilité de modifier en fonction de ce u'on me passe comme url? 

    // un outil tu lui passes une trentaine de liens tu fais ton truc avec l'api de youtube.
    // ça te renvoie un json et la directos tu fais ton truc;

//  Pour calculer le pourcentage de like par vidéo on utilise le nombres de vue
//  le nombres de dislike est multiplé par deux car je pars de l'hypothèse qu'il faut être pas mal contrarié pour appuyer sur le pouce rouge
//  ce nombres est ensuite soustrait au nombre de like inital pour chaques vidéo 
//  je divise ensuite ce nombre par le nombre de vue puis je multiplie par 100 pour avoir le ratio en pourcentage
//  je fais la même chose pour le nombre de commentaire 
// Les deux videos qui ont le max et le min de vues sont idéntifiés.
// en fonction de celle ci j'extrait la différence 
// Pour chaques visoe je vais ensuite soustraire le nombre de vue minimum à celui de la video puis diviser le tout par le max de vue puis multiplier par le nombre en mm 
// screenshot de leur face pc grimacant sur thumbnails 

const browseData = (data) => {
    Object.keys(data.urls).forEach(item => {
        const title = data.urls[item].snippet.title
        const viewCount = parseInt(data.urls[item].statistics.viewCount, 10)
        const likeCount = parseInt(data.urls[item].statistics.likeCount, 10)
        const dislikeCount = parseInt(data.urls[item].statistics.dislikeCount, 10)
        const commentCount = parseInt(data.urls[item].statistics.commentCount, 10)
        arrayNumberViews.push(viewCount)
        const commentPourcentage = calculPourcentageViewCount(viewCount, commentCount, 0)
        arrayCommentPourcentage.push(commentPourcentage)
        const likePourcentage = calculPourcentageViewCount(viewCount, likeCount, dislikeCount)
        createNewObjectVideo(title, viewCount, likePourcentage, commentPourcentage)
    });
    // SORT BY LIKE POURCENTAGE
    allVideosInfo.sort(compareLikePourcentage)
    // MAX AND MIN NUMBER OF VIEUW
    maxAndMin()
    // fetchAsciiHtml()
    constructHtml()
}


const maxAndMin = () => {
    maxViewsNumber = Math.max(...arrayNumberViews)
    minViewsNumber = Math.min(...arrayNumberViews)
    maxCommentPourcentage = Math.max(...arrayCommentPourcentage)
    minCommentPourcentage = Math.min(...arrayCommentPourcentage)


    allVideosInfo.forEach(obj => {
        if (obj.viewCount === maxViewsNumber) {
            obj.maxViews = true
        }
        if (obj.commentPourcentage === maxCommentPourcentage) {
            obj.maxComment = true
        }
        if (obj.viewCount === minViewsNumber) {
            obj.minViews = true
        }
        if (obj.commentPourcentage === minCommentPourcentage) {
            obj.minComment = true
        }
    });
}

const calculPourcentageViewCount = (viewCount, likeCount, dislikeCount) => {
    const pourcentageLikes = parseFloat((((likeCount - dislikeCount * 2) / (viewCount)) * 100))
    return pourcentageLikes
}


const createNewObjectVideo = (title, viewCount, likePourcentage, commentPourcentage) => {
    const videosInfo = {
        "title": title,
        "viewCount": viewCount,
        "likePourcentage": likePourcentage,
        "commentPourcentage": commentPourcentage
    }
    allVideosInfo.push(videosInfo)
}


const compareLikePourcentage = (a, b) => {
    if (a.likePourcentage < b.likePourcentage) {
        return -1;
    }
    if (a.likePourcentage > b.likePourcentage) {
        return 1;
    }
    return 0;
}


const constructHtml = () => {
    maxHeight = maxViewsNumber - minViewsNumber
    allVideosInfo.forEach((obj, index) => {
        const size = (((obj.commentPourcentage - minCommentPourcentage) / maxCommentPourcentage) * 100)
        const positionHeight = (((obj.viewCount - minViewsNumber) / maxViewsNumber) * 550)
        const positionWidth = (((obj.likePourcentage - allVideosInfo[0].likePourcentage) / allVideosInfo[allVideosInfo.length - 1].likePourcentage) * 350)

        const newDiv = document.createElement("div");
        const newContent = document.createTextNode(`${obj.title}`);
        newDiv.appendChild(newContent);
        newDiv.setAttribute("class", "images-data");
        newDiv.style.bottom = `${positionHeight.toFixed(0)}mm`
        newDiv.style.left = `${positionWidth.toFixed(0)}mm`
        newDiv.style.width = `${size.toFixed(0)}mm`
        newDiv.style.height = `${size.toFixed(0)}mm`
        newDiv.style.backgroundImage = `url(./faces/face${index}.png)`
        newDiv.setAttribute("id", `div${index}`);
        const currentDiv = document.getElementById("data-graphic");
        currentDiv.appendChild(newDiv);

    });
}


//ASCII

// const fetchAsciiHtml = () => {
//     for (let i = 1; i < 30; i++) {
//     fetch(`${window.location.href}/ascii/asciiHtml/html${i}.html`)
//         .then(response => response.text())
//         .then((data) => {
//             console.log("data", typeof data)
//             constructAsciiHtml(data)
//         });
// }
// }
// const constructAsciiHtml = (data) => {
//     maxHeight = maxViewsNumber - minViewsNumber
//     allVideosInfo.forEach((obj, index) => {
//         const size = (((obj.commentPourcentage - minCommentPourcentage) / maxCommentPourcentage) * 400)
//         const positionWidth = (((obj.viewCount - minViewsNumber) / maxViewsNumber) * 700)
//         const positionHeight = (((obj.likePourcentage - allVideosInfo[0].likePourcentage) / allVideosInfo[allVideosInfo.length - 1].likePourcentage) * 1000)

//         const newDiv = document.createElement("div");
//         const newContent = document.createTextNode(`${obj.title}`);
//         newDiv.appendChild(newContent);
//         newDiv.setAttribute("class", "images-data");
//         newDiv.style.bottom = `${positionHeight.toFixed(0)}mm`
//         newDiv.style.left = `${positionWidth.toFixed(0)}mm`
//         newDiv.style.width = `${size.toFixed(0)}mm`
//         newDiv.style.height = `${size.toFixed(0)}mm`
//         newDiv.innerHTML += `${data}`;
//         // newDiv.style.backgroundImage = `url(./faces/face${index}.png)`
//         newDiv.setAttribute("id", `div${index}`);
//         const currentDiv = document.getElementById("data-graphic");
//         currentDiv.appendChild(newDiv);

//     });
// }


