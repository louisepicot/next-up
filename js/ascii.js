// Dependencies
var fs = require('fs')
var path = require('path');
const imageToAscii = require("image-to-ascii");

var Convert = require('ansi-to-html');
var convert = new Convert();


var path = "./faces";

fs.readdir(path, function (err, files) {
    if (err) {
        console.error("Could not list the directory.", err);
        process.exit(1);
    }
    console.log(files);
    files.forEach(function (file, index) {

        imageToAscii(`./faces/${file}`, (err, converted) => {
            console.log(err || converted);

            let convertHtml = convert.toHtml(`${converted}`)
            fs.writeFile(`./ascii/asciiHtml/html${index}.html`, '', function () { console.log('clean') })
            fs.appendFile(`./ascii/asciiHtml/html${index}.html`, `<pre>${convertHtml}</pre>`, function (err) {
                if (err) {
                    // append failed
                } else {
                    console.log("done")
                }
            })
        });
    });

});










