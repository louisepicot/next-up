

const apiKey = "AIzaSyDnhimZeYtDZ_CnZoCQgxRvwaCs-3bk-8I"
const input = document.querySelector("#url")
const form = document.querySelector("form")
let arrayInfo = []
let data = [],
    arrayNumberViews = [],
    ratingLikes = [],
    allVideosInfo = [],
    heightDisplay = 100,
    maxViewsNumber,
    minViewsNumber,
    count = 1,
    arrayCommentPourcentage = [];
let commentclicked = false

form.addEventListener("submit", e => {
    e.preventDefault()

    const urlArray = input.value.split("https").map(url => {
        url = url.replace(/"/g, '')
        url = url.replace(",", '');
        url = url.replace(/\s/g, '')
        return url
    }).filter(url => url !== "")

    console.log(urlArray)
    urlArray.forEach(url => {
        const urlId = retrieveIdFromInput(url)
        if (urlId) {
            fetchYoutubeData(urlId)
        }
    })

    // // SORT BY LIKE POURCENTAGE
    setTimeout(() => {
        allVideosInfo.sort(compareLikePourcentage)
        // // MAX AND MIN NUMBER OF VIEUW
        maxAndMin()
        // // fetchAsciiHtml()
        constructHtml()
    }, 1000);

})


const retrieveIdFromInput = (url) => {

    const regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    const match = url.match(regExp);
    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return false
    }
}


const fetchYoutubeData = (inputUrl) => {
    fetch(`https://www.googleapis.com/youtube/v3/videos?id=${inputUrl}&key=${apiKey}&part=snippet,contentDetails,statistics,status`)
        .then(response => response.json())
        .then((data) => {
            browseData(data)
        });
}


const browseData = (data) => {
    console.log(data)

    const thumbnails = data.items[0].snippet.thumbnails.default.url
    const title = data.items[0].snippet.title
    const viewCount = parseInt(data.items[0].statistics.viewCount, 10)
    const likeCount = parseInt(data.items[0].statistics.likeCount, 10)
    const dislikeCount = parseInt(data.items[0].statistics.dislikeCount, 10)
    const commentCount = parseInt(data.items[0].statistics.commentCount, 10)
    arrayNumberViews.push(viewCount)

    const commentPourcentage = calculPourcentageViewCount(viewCount, commentCount, 0)
    arrayCommentPourcentage.push(commentPourcentage)
    const likePourcentage = calculPourcentageViewCount(viewCount, likeCount, dislikeCount)
    createNewObjectVideo(title, viewCount, likePourcentage, commentPourcentage, thumbnails)
}

const createNewObjectVideo = (title, viewCount, likePourcentage, commentPourcentage, thumbnails) => {
    const videosInfo = {
        "title": title,
        "thumbnails": thumbnails,
        "viewCount": viewCount,
        "likePourcentage": likePourcentage,
        "commentPourcentage": commentPourcentage
    }
    allVideosInfo.push(videosInfo)
}


const maxAndMin = () => {
    maxViewsNumber = Math.max(...arrayNumberViews)
    minViewsNumber = Math.min(...arrayNumberViews)
    maxCommentPourcentage = Math.max(...arrayCommentPourcentage)
    minCommentPourcentage = Math.min(...arrayCommentPourcentage)


    allVideosInfo.forEach(obj => {
        if (obj.viewCount === maxViewsNumber) {
            obj.maxViews = true
        }
        if (obj.commentPourcentage === maxCommentPourcentage) {
            obj.maxComment = true
        }
        if (obj.viewCount === minViewsNumber) {
            obj.minViews = true
        }
        if (obj.commentPourcentage === minCommentPourcentage) {
            obj.minComment = true
        }
    });
}

const calculPourcentageViewCount = (viewCount, likeCount, dislikeCount) => {
    const pourcentageLikes = parseFloat((((likeCount - dislikeCount * 2) / (viewCount)) * 100))
    return pourcentageLikes
}




const compareLikePourcentage = (a, b) => {
    if (a.likePourcentage < b.likePourcentage) {
        return -1;
    }
    if (a.likePourcentage > b.likePourcentage) {
        return 1;
    }
    return 0;
}


const constructHtml = () => {
    console.log("max and min", maxViewsNumber, minViewsNumber)
    console.log("pourcentage", minCommentPourcentage, maxCommentPourcentage)
    maxHeight = maxViewsNumber - minViewsNumber
    allVideosInfo.forEach((obj, index) => {
        let size;
        if (obj.commentPourcentage > minCommentPourcentage) {
            size = (((obj.commentPourcentage - minCommentPourcentage) / maxCommentPourcentage) * 200)
        } else {
            size = ((minCommentPourcentage) * 200)
        }
        const positionHeight = (((obj.viewCount - minViewsNumber) / maxViewsNumber) * 80)
        const positionWidth = (((obj.likePourcentage - allVideosInfo[0].likePourcentage) / allVideosInfo[allVideosInfo.length - 1].likePourcentage) * 80)
        console.log(obj.commentPourcentage, size, positionHeight, positionWidth)
        const newDiv = document.createElement("div");
        const newContent = document.createTextNode(`${obj.title}`);
        newDiv.appendChild(newContent);
        newDiv.setAttribute("class", "images-data");
        newDiv.style.bottom = `${positionHeight}%`
        newDiv.style.left = `${positionWidth}%`
        if (commentclicked) {
            newDiv.style.width = `${size}px`
            newDiv.style.height = `${size}px`
        } else {
            newDiv.style.width = `160px`
            newDiv.style.height = `160px`
        }


        newDiv.style.backgroundImage = `url(${obj.thumbnails})`
        newDiv.setAttribute("id", `div${index}`);
        const currentDiv = document.getElementById("data-graphic");
        currentDiv.appendChild(newDiv);

    });
}

document.querySelector("#commentBtn").addEventListener("click", e => {
    commentclicked = !commentclicked
})
// passer une liste de liens youtube (min 3)
// pour chaque liens de la liste on traite les info et on affiche le thumbnails avec les couleurs palettes à un endroit de la page 



// test
// https://www.youtube.com/results?search_query=lost+frequencies
// https://www.youtube.com/watch?v=wkCGjEBNdxI
// https://www.youtube.com/watch?v=E90eZc1ndCw
// https://www.youtube.com/watch?v=gYeWpJD8g8g
// https://www.youtube.com/watch?v=6kfE9GQN-h0
// https://www.youtube.com/watch?v=s9LTuz8IHLY
// https://www.youtube.com/watch?v=BtyHYIpykN0
// https://www.youtube.com/watch?v=Vl-GJaitlNs
// https://www.youtube.com/watch?v=Tf0cJGO52xc
