// TOOLS
const colorThief = new ColorThief();
const img = new Image();
const imgAll = document.querySelectorAll(".thumbnail > img")
let googleProxyURL = 'https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&refresh=2592000&url=';

imgAll.forEach(img => {
    let imageURL = `${img.attributes.src.value}`;
    img.crossOrigin = 'Anonymous';
    img.src = googleProxyURL + encodeURIComponent(imageURL);
    img.addEventListener('load', function () {
        let palette = colorThief.getPalette(img);
        let mainColors = colorThief.getColor(img);
        palette.forEach((pal) => {
            let hexPal = rgbToHex(pal[0], pal[1], pal[2])
            constructionColorsScale(hexPal, img)
        })
        mainColors = rgbToHex(mainColors[0], mainColors[1], mainColors[2])
        constructionColorsScale(mainColors, img)
    })
})


const constructionColorsScale = (colors, img) => {
    const newDiv = document.createElement("div");
    newDiv.setAttribute("class", "colors");
    newDiv.style.background = `${colors}`
    let palettesDiv;
    if (count <= 7) {
        palettesDiv = document.querySelector("#palettes1")
    }
    if (count >= 7 && count <= 14) {
        palettesDiv = document.querySelector("#palettes2")
    }

    if (count >= 14 && count <= 21) {
        palettesDiv = document.querySelector("#palettes3")
    }
    if (count >= 21 && count <= 28) {
        palettesDiv = document.querySelector("#palettes4")
    }

    // const parentDiv = palettesDiv;
    palettesDiv.appendChild(newDiv)
    // const parentDiv = img.parentNode;

    // parentDiv.insertBefore(newDiv, img);
    count++
}

const rgbToHex = (r, g, b) => '#' + [r, g, b].map(x => {
    const hex = x.toString(16)
    return hex.length === 1 ? '0' + hex : hex
}).join('')

